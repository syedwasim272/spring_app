package com.springboot.cgcs.controller;

import java.util.List;

import com.springboot.cgcs.entities.Post;
import com.springboot.cgcs.services.PostServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostsController {

    @Autowired
    private PostServices postService;

    @GetMapping("/posts")
    public List<Post> getPosts() {
        return this.postService.getPosts();
    }

}
