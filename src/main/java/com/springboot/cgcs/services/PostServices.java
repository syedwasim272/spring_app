package com.springboot.cgcs.services;

import java.util.List;

import com.springboot.cgcs.entities.Post;

public interface PostServices {
    public List<Post> getPosts();
}
