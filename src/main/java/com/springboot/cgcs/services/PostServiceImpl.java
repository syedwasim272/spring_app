package com.springboot.cgcs.services;

import java.util.ArrayList;
import java.util.List;

import com.springboot.cgcs.entities.Post;

import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostServices {

    List<Post> list;

    public PostServiceImpl() {
        list = new ArrayList<>();
        list.add(new Post(1, 1, "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"));
        list.add(new Post(1, 2, "qui est esse",
                "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"));
    }

    @Override
    public List<Post> getPosts() {
        // TODO Auto-generated method stub
        return list;
    }

}
